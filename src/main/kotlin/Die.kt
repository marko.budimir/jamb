import kotlin.random.Random

data class Die(
    var number: Int = 0,
    private var isLocked: Boolean = false
){

    fun throwDie(){
        if(!isLocked){
            number = Random.nextInt(1, 6)
        }
    }

    fun lockDie(){
        isLocked = true
    }

    fun unlockDie(){
        isLocked = false
    }
}
