class Player(val id: Int, diceNumber: Int) {
    private val dice: MutableList<Die> = mutableListOf()
    var score : Int = 0

    init {
        for(i in 1..diceNumber){
            dice.add(Die())
        }
    }

    fun throwDice(){
        for(die in dice){
            die.throwDie()
        }
    }

    fun getDiceNumbers(): IntArray{
        val diceNumbers = IntArray(dice.size)

        for(i in 1..dice.size){
            diceNumbers[i-1] = dice[i-1].number

        }
        return diceNumbers
    }

    fun lockDice(positions: IntArray?){
        positions?.let {
            for (position in positions) {
                if((position>0) and (position <= dice.size)){
                    dice[position-1].lockDie()
                }
            }
        }
    }

    fun unlockDice(positions: IntArray?){
        positions?.let {
            for (position in positions) {
                if((position>0) and (position <= dice.size)){
                    dice[position-1].unlockDie()
                }
            }
        }
    }
}