package Checkers

class YahtzeeChecker : ICheckable{
    override val value: Int
        get() = 50

    override fun check(numbers: IntArray): Boolean {
        var count: Int
        for(number in numbers.distinct()){
            count = numbers.count { it == number }
            if((count == 5) or (count == 6)){
                return true
            }

        }
        return false
    }
}