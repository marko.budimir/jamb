package Checkers

class PokerChecker : ICheckable{

    override val value: Int
        get() = 40

    override fun check(numbers: IntArray): Boolean {
        for(number in numbers.distinct()){
            if(numbers.count { it == number } == 4){
                return true
            }

        }
        return false
    }
}