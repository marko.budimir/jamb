package Checkers

class SmallStraightChecker : ICheckable {
    private val smallStraight = arrayOf(1, 2, 3, 4, 5)
    override val value: Int
        get() = 30

    override fun check(numbers: IntArray): Boolean {

        val intersectNumbers = numbers intersect smallStraight.toList().toSet()
        return intersectNumbers.size == 5
    }
}