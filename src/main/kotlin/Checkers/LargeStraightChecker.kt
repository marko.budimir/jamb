package Checkers

class LargeStraightChecker: ICheckable {
    private val largeStraight = arrayOf(2, 3, 4, 5, 6)
    override val value: Int
        get() = 40

    override fun check(numbers: IntArray): Boolean {
        val intersectNumbers = numbers intersect largeStraight.toList().toSet()
        return intersectNumbers.size == 5
    }
}