package Checkers

interface ICheckable {
    val value: Int
    fun check(numbers: IntArray) : Boolean
}