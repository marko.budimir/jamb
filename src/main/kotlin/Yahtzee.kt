import Checkers.ICheckable

class Yahtzee(playerNumber: Int, checkers: MutableList<ICheckable>) {
    private val NUMBER_OF_DICE = 6
    private val DICE_POSITIONS = intArrayOf(1, 2, 3, 4, 5, 6)

    private val players = mutableListOf<Player>()
    private val checkers = mutableListOf<ICheckable>()

    init {
        for(i in 1..playerNumber){
            players.add(Player(i, NUMBER_OF_DICE))
        }
        this.checkers.addAll(checkers)
        checkers.sortByDescending { it.value }
    }
    fun playRound(){
        for(player in players){
            println("--------------------------------------------")
            println("Player ${player.id}:")
            for(i in 1..3) {
                println("Throw $i:")
                player.throwDice()
                val diceNumbers = player.getDiceNumbers()
                for (j in 0..5) {
                    println("Dice ${j + 1}: ${diceNumbers[j]}")
                }
                if(i<3) {
                    println("Enter 0 or number of dice you want to lock")
                    val lockedDicePositions = readLine()
                    if (lockedDicePositions != "") {
                        val positions = lockedDicePositions?.split(" ")?.map { it.toInt() }?.toIntArray()
                        player.lockDice(positions)
                    }
                }
                else{
                    player.score += checkCombination(diceNumbers)
                    println("Player ${player.id} score: ${player.score}")
                    player.unlockDice(DICE_POSITIONS)
                }
            }
            Thread.sleep(1000)
        }
    }
    private fun checkCombination(diceNumbers: IntArray): Int{
        for(checker in checkers){
            if(checker.check(diceNumbers)){
                return checker.value + diceNumbers.sum()
            }
        }
        return 0
    }

    fun getScore(): String{
        val stringBuilder: StringBuilder = StringBuilder()
        stringBuilder.append("Score:\n")
        for(player in players){
            stringBuilder.append("Player ${player.id}:").append(" ${player.score}\n")
        }
        return stringBuilder.toString()
    }

    fun getWinner(): Int{
        val maxScore = players.maxOf { it.score }
        return players.first{it.score == maxScore}.id
    }

    fun restartScore(){
        for(player in players){
            player.score = 0
        }
    }

}