import Checkers.*

fun main(args: Array<String>) {
    val checkers: MutableList<ICheckable> = mutableListOf()
    checkers.add(SmallStraightChecker())
    checkers.add(LargeStraightChecker())
    checkers.add(PokerChecker())
    checkers.add(YahtzeeChecker())

    println("Enter number of players")
    val playerNumber = readLine()?.toInt()
    playerNumber?.let {
        val yahtzee = Yahtzee(playerNumber, checkers)

        yahtzee.playRound()
        println(yahtzee.getScore())
        yahtzee.playRound()
        println(yahtzee.getScore())
        println("The winner is player ${yahtzee.getWinner()}")
        yahtzee.restartScore()
    }
}